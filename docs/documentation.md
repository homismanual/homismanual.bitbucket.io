# GoT-HoMIS USER GUIDE

## 1. General Information
Health facilities are places where health care is provided. At the bottom there are the dispensaries found in every village where the village leaders have a direct influence in its running. The health centres are found at ward level and the health centre in charge is answerable to the ward leaders. At the district there is a district hospital and at regional level a regional referral hospital. The tertiary level is usually the zone hospitals and at national level there is the national hospital. There are also some specialized hospitals which do not fit directly into this hierarchy and therefore are directly linked to the ministry of health.

## 1.1. Scope
this manual applies to all health sectors such as regional hospital, district hospital, health centre and Dispensary.
  

## 1.2. SYSTEM OVERVIEW

Government of the United Republic of Tanzania has acknowledged the importance of the use of technology in the health facilities to make them work effectively and with efficiency by changing the entire hospital process from paper to paper less.

  President’s Office – Regional Administration and Local Government (PO-RALG) in collaboration with the Ministry of Health, Community Development, Gender, Elderly and Children, Kibaha Education Center (KEC) and the University of Mzumbe has come up with a software solution for hospital management named Government of Tanzania Hospital Management Information system (GoT-HoMIS). 

  GoT-HoMIS was initiated by the Kibaha Education Center (KEC) and Mzumbe University, GoT-HoMIS is developed using local expertise within the country. Since its inception back in 2015. GoT-HoMIS has gone through version transitions and is now in v3. GoT-HoMIS is an integrated information system that covers the aspects of Electronic Medical Record (EMR), management of MTUHA reports, Billing and recording, Laboratory Information system and inventory management. Under EMR the system comprises of patient Registration, OPD, IPD, Operating theatre, Blood bank, Mortuary, Emergency and casualty. 

  Under MTUHA the system creates certain information and make the information mandatory during registration process so that a report can be sent to MTUHA. For Billing and recording GoT-HoMIS does electronic payment through the use of Government Electronic Payment Gateway (GePG) In Laboratory information system, the system automate the investigations requests and process involved in delivering the concerned department of the health facility.

   Under inventory management GoT-HoMIS provides functionality for requisition of medical supplies, purchase of items, stock management, automatic reorder level settings, online request for stock from main store to sub-store and dispensing points. The system control medical flow from MSD to respective patient.


## 2. SYSTEM REQUIREMENTS.

The hardware and software requirements specifications enlist necessary requirements that are required to accomplish the process of system installation. Any system installation needs a conducive environment for its successful installation. Before installing a system, system requirements needs to be available in order to ease the installation process. Before installing GoT-HoMIS there must be the following pre-requisites.

## 2.1. Server Software Requirements
GoT-HoMIS requires a few software on a server machine for it to operate smoothly. Although the system can be installed and run on a normal desktop/laptop computer with decent specifications, a Health Facility shall require a “server” dedicated to host the system in the facility Local Area Network (LAN). 
The following are software requirements on the server

+ Modern Server Operating system (Linux/Window)
+ Web Server:- Apache
+ PHP Engine
+ MySQL Database (This and the above two can all be installed once by the  XAMPP/LAMPP package)
+ An editor on Windows platform (Recommended Notepad++)
+ PHP Dependence Manager (Composer)

## 2.2. Server Hardware Requirements
- RAM at least 8GB
- Hard disk at least 1 Terabyte
- Internet Connection

## 2.3. Client Software Requirements
Since GoT-HoMIS is a web based software, the only software required on the client is an up to date Chrome Browser.

## 2.4. Client Hardware Requirements

+ At least 2GB RAM on Clients computers
+ Receipt Thermal printer (Preferably Epson …) 
+ (Small sticker) Barcode printer
+ Barcode reader

## 2.5. INSTALLATION
The installation process is as follows;

1.	Install XAMPP
2.	Install browser e.g chrome
3.  Install notepad ++
4.	Install composer
5.	Copy folder containing  GoT-HoMIS V3 and paste it to C:\xampp\htdocs\got_homis_v3
6.	Open terminal/CMD
    + Change directory to root  i.e cd /
    + Then go to xampp/mysql/bin

![](img/cmdone.jpg)

System will show the loading bar with progress on the top of the page. After finishing the activation system will show “SYSTEM DABABASE ACTIVATED”.


   + Create database

![](img/cmdtwo.jpg)


7.  Make sure xampp is turned on
8.  Go to htdocs, open the folder containing the database, go to .env file and edit it with notepad++, then change the database name to ensure it is similar to the name of a    database you created.
9.  Configure and clear cache. The cache is cleared and configured as follows;

![] (img/cache.jpg)

10. Database Migration and Seeding
    + Database Migration is the process of creating the database schema on the database name created in step (vi) above.
    + Database seeding is a simple way to add initial data sets into your database.

Database Migration steps:

![](img/migration.jpg)

Port assigning and virtual host declaration:

If apache cannot be started on the default port (80), assign new port numbers as follows:
Open the folder; C:\xampp\apache\conf

Under config, open httpd.config with Notepad C++ then add or edit port numbers under the Port Listening configuration part.

For example 

![](img/assignport.jpg)

Then open folder …..Extra:  C:\xampp\apache\conf\extra

For example

![](img/virtualhost.jpg) 

Line 36 - 42 - Virtual Host with listening port number 7070.

Line 37:     - Administrator Server email address.

Line 38:     - Folder name where system file for the domain name are Stored.Line 39:     - Local IP Address of the server where the system will be launched.

Line 40:    - Contains a record of critical errors that occurred during the   
                     Server operation.

 Line 41:    - Create custom domains for development in XAMPP.


<span style = "color:red">_**Restart apache and make sure the port numbers you have assigned are visible on the Apache Service port numbers on the XAMPP panel**_</span>

# 3.0.Getting started

## 3.1. Login
The system provides the user with default Username and Password.After the first login, the system allows the user to change default Password by choosing password of their choises.

## 3.2. Login into GoT-HoMIS

1. Open the browser and type the IP address followed by full colon (:) 
for example <span style = "color:red"> 127.0.0.1:7070 </span>


## login page

![](img/loginpage.jpg)

# 3.1. Introduction to (GoT – HoMIS) Modules

A module is a software component or part of a program that contains one or more routines. One or more independently developed modules make up a complete program. Modules are categorized according to the functionalities. User role can be assigned/removed to a user according to the user privileges. 

## 3.2.ADMINISTRATOR

Super user/super admin is the person in charge of all other users in the system. By default, the user who signs up first and creates the organization is the Super Administrator. The Super Administrator has the utmost privileges across the entire organization. Super user is capable of: Change the role of a User to Admin and vice versa. View, manage or Change the details for any organization. There can be only one Super Admin in the entire organization.

## 3.2.1. Administrator login

Administrator must login in to the system with the valid credentials. By Default username = admin@tamisemi.go.tz and password = 12345678.
![](img/administratorlogin.jpg)


## 3.2.2. System Activation
After login successfully Administrator activate system Views. System views are MySQL views which control different logics and process in the system. To activate system views click on DB Setup then open system activation Tab, on system activation Tab click the button ACTIVATE. As shown below.

<span style = "color:red">_**Note:
System database activation is compulsory. It is a very important step because it enable items sales and user details account to be activated
**_</span>

Open DB_Setup
![](img/dbsetup.jpg)

Activate view
![](img/activateview.jpg)

System will show the loading bar with progress on the top of the page. After finishing the activation system will show “SYSTEM DABABASE ACTIVATED”.

<span style = "color:red">_**NOTE: System views must be activated only once.**_</span>

## 3.2.3. Create Facility 
For the facility to be formulated, the administrator must do the following;

1. Open the DB Setup module
2. Click on facility registration tab and register the facility details: Facility code, name, Address, email, phone number, facility type, council and region. 

![](img/facilityregistration.jpg)

## 3.3. PHARMACY

Pharmacy is a module in a system which is responsible for managing and dispensing drugs. It is the module which focus on safe and effective medication use. The system allow Pharmacist to inform patients in all aspects of their medicine including recommending types as well as administration route and dosages. Hospital pharmacist are responsible for monitoring the supply of all medicines used in the hospital and are in charge of purchasing, dispensing and quality testing their medication stock.
Pharmacy contains sub modules such as

1. Item setup
2. Main pharmacy
3. Sub store 
4. Dispensing

## 3.3.1. Item Setup
![](img/itemsetup.jpg)

Item setup enables users to register all items which are available in a facility. 
Item setup consist of;
**_Item price_,_Registration_,_Change item category_,_Item Category_,_SErvice config,_Exemption status_,_sub department_,_Reception Service_,_Diagnosis Registry_**


**3.3.1.1.Item registration**

1. enter the item name 
2. choose item department.
3. Click register

![](img/itemregistration.jpg)


**3.3.1.2.Item mapping**

1. Search the name of the registered item
2. Choose the item category in order to map the registered item to a correct item category.
3. Click register
![](img/itemmapping.jpg)

**3.3.1.3.Item sub department Registry **

1. Search the item
2. choose sub department Registry of that item.
3. Register

![](img/subdepartmentregistry.jpg)

**3.3.1.4.Item sub department List **

After items registration the registered items with their corresponding sub departments are listed on this sub module. on this part the system has the edit and delete options.

**3.3.1.5.Item List**

All registered items with the corresponding departments are listed here

![](img/itemlist.jpg)

**3.3.1.6.Item price**
Item price consist of;*Create item price* and *Item price list*

**3.3.1.7.Create item price**

In this sub module,the price of all items are set according to the payment categories.

1. Search the item
2. Enter the price of items according to the payment category. 

![](img/itemprice.jpg)

**3.3.1.8.Item Price List**

The created items with their prices are listed on item priced list.
![](img/itempricelist.jpg)

**3.3.1.9.Change item category**

in this sub module,Items assigned with incorrect categories are edited here by searching an item and choosing its correct category.

![](img/changeitemcategory.jpg)

**3.3.1.10.Item Category**

under this sub module the registered items are listed where the system gives an option to edit/delete the item.

![](img/itemcategory.jpg)

*3.3.1.11.Service Config**




**3.3.1.12.Exemption Status**

Items in a facility have different status, some items are considered as exempted items and others do not have exemption, thus requires a client to make payment for such an item.
System allows the search of items and assign it as an exemption item or to remove it from exemption item list.


![](img/exemptionstatus.jpg)

**3.3.1.13.Sub department**

This sub module enables sub departments to be registered and after registration the registered sub departments are listed at the right side of the page.
Sub departments are important for measuring performance and for report preparation.

![](img/subdepartmentregistration.jpg)

**3.3.1.14.Reception Services**

It gives an option to search and to save services that needs to be viewed by the receptionist during patient/client registration.

![](img/receptionservices.jpg)

**3.3.1.15.Diagnosis registry**

This part allows the registration of diagnosis and diagnosis code. After registration these diagnosis are viewed by the doctor and the doctor is able to assign these diagnosis to a patient.

**3.3.1.16.Payment Category Setup**

Payment category is a module responsible for setting the payment sub-categories from User fee, Insurance or Exemption categories. After registering the payment subcategories, those payments will be listed in the patient payment category list

![](img/paymentcategorysetup.jpg)

## 3.4. MAIN STORE (MAIN PHARMACY)

Main pharmacy consist of, setting Requisitions, Item record, receiving and issuing.

![](img/mainpharmacy.jpg)

## 3.4.1. Setting

In this sub module the system is capable of creating store, vendors, invoices, store access, Tracer medicine and Transaction types

## 3.4.1.1 Create new store

1. click create a new store
2. Enter store name
3. Select store type
4. click Register for new store registration.
![](img/storecreation.jpg)

## 3.4.1.2 Create new Vendor

1. Click Create a new vendor
2. Enter the required details i.e; Vendor name, phone number,address and contact person
3. click Register for new Vendor registration.
![](img/vendorcreation.jpg)

## 3.4.1.3Create new Invoice
1. click create a new Vendor
2. Enter invoice number
3. Select Vendor name

## 3.4.1.4 Store access

Store access are given to responsible users of a certain store by;

1. Search user 
2. Assign user to a store access
3. save

![](img/storeaccess.jpg)

## 3.4.1.5 Tracer medicines

This part allows the tracer medicine to be registered by;

1. searching the specific items to be traced 
2. select the item status the status of that items.(status can be whether the item is available or not available in the facility.) 
3. if there is more than one item, add all items to list
4. Register
![](img/tracermedicine.jpg)

## 3.4.1.5 Transaction Type

All transaction types used in receiving and issuing are registered here.

1. Enter transaction type name
2. Select adjustment type(adjustment can be positive or negative)

![](img/transactiontype.jpg)

## 3.4.2 Setting summary

 This part lists store list,vendor list and invoice list.
![](img/settingsummary.jpg)

3.4.3. Requisition

In requisition is where the incoming requisition can be viewed(the quick search button allows the search for a specific requisition) and new requisition is created.
The system allows all requisitions to be added on the list then the items are saved at once.
![](img/createquisition.jpg)

![](img/incomingrequisition.jpg)






















 
























